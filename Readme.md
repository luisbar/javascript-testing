## What is this?
A monorepo which contains useful examples about testing Javascript projects in both frontend and backend.

### 1. Basis
A package which contains code that will give you an idea how Jest is implemented.

You can run executing the following sentence:
```
npm run -w basis test
```

### 2. Mocks
A package which contains a mock implementation which will give you an idea about how the `jest.mock` and the `jest.fn` methods work.

You can run executing the following sentence:
```
npm run -w mocks test
```

### 3. Static analysis testing
We can use the following tools for static analysis:

- Eslint: fix our code based on our defined Eslint rules
  - You can install a Eslint extension or plugin for your editor
  - We can ignore files that we are ignoring in our `.gitignore` file, just you have to add `eslint --ignore-path .gitignore`
  - Install `eslint-config-prettier` in order to avoid problems with Prettier configuration, and add it into the `extend` property
  - If we want to use Eslint with Typescript we need to do the following:
    - Install `@typescript-eslint/eslint-plugin` and `@typescript-eslint/parser`
    - Add the `--ext .js,.ts,.tsx` option to Eslint command
    - Add the the following configuration to our Eslint config file
    ```json
    {
      "overrides": [{
        "files": "**/*.+(ts|tsx)",
        "parser": "@typescript-eslint/parser",
        "parserOptions": {
          "project": "./tsconfig. json",
        },
        "plugins": ["@typescript-eslint/eslint-plugin"],
        "extends": [
          "plugin: @typescript-eslint/eslint-recommended",
          "plugin: @typescript-eslint/recommended",
          "eslint-config-prettier/@typescript-eslint",
        ],
      }]
    }
    ```
- Prettier: format our code based on our Prettier config
  - We can ignore files that we are ignoring in our `.gitignore` file, just you have to add `prettier --ignore-path .gitignore`
  - Check [Prettier playground](https://prettier.io/playground/)
  - You can install a Prettier extension or plugin for your editor
  - You can use the double dash `--` to reuse the Prettier command as follow:
    ```
      "prettier": "prettier --ignore-path .gitignore \"**/*.+(js|json)\"",
      "format": "npm run prettier -- --write",
      "check-format": "npm run prettier -- --list-different",
    ```
- Husky
  - You can bypass the Husky hook by passing `--no-verify` at the end of the commit command
- Lint Staged
- Npm Run All

### 4. Introduction DOM testing
A package which contains a basic example of DOM testing by using Vanilla JS

You can run executing the following sentence:
```
npm run -w introduction-dom-testing test
```
### 5. Jest configuration
Here you can find some useful tips for configuring Jest

- If you want to remove unused snapshots files you can run
  ```
  npm test -- -u
  ```
- If you want to include your styles in your snapshot, you need to install and configure `jest-emotion`
- In order to import module as if they were in `node_modules`, you can use, `modulesDirectories` property on `jest.config.js` file
  - If Eslint is complaining about these 'aliases', you can install and configure `eslint-import-resolver-jest`
  ```
  "overrides": [
    {
      "files": ["**/__tests__/**/*.js"],
      "settings": {
        "import/resolver": {
          "jest": {
            "jestConfigFile": "./jest.conf.json"
          }
        }
      }
    }
  ]
  ```
- You can execute Jest in watch mode by using the following command
  ```
  jest --watch
  ```
- You can use the debug mode of node for running test by using the following command
  ```
  node --inspect-brk ./node_modules/jest/bin/jest.js --runInBand --watch
  ```
  After that you should set a breakpoint in your code by using the `debugger` command, run your tests and open the Chrome inspector by settings this in your browser `chrome://inspect`
- For enabling the code coverage report, you have to add the `--coverage` option to the jest command and define the `collectCoverageFrom` property in your `jest.config.js` file (e.g. `collectCoverageFrom: **/src/**/*.js`)
- For checking if we cover all the lines of our code, Jest uses [babel-plugin-istanbul](https://github.com/istanbuljs/babel-plugin-istanbul)
- If you want to ignore some lines from code coverage you can add the following line to your code
  ```
  /*istanbul ignore next*/
  ```
- If you want to stablish a threshold for your code coverage you should add the following configuration
  ```
  {
    coverageThreshold: {
      global: {
        statements: 100,
        branches: 100,
        functions: 80,
        lines: 80,
      },
      './PATH/file.js': {
        statements: 100,
        branches: 100,
        functions: 100,
        lines: 100,
      },
    },
  }
  ```
- You can use [Codecov](https://about.codecov.io) for pushing your code coverage data
- If you want to run Jest in watch mode locally and run Jest with code coverage in you CI without explicitly running each command, you can use the [is-ci-cli](https://github.com/YellowKirby/is-ci-cli) library. This library uses [is-ci](https://github.com/watson/is-ci) and this uses [ci-info](https://github.com/watson/ci-info)
- If you want to have different Jest config files, you can use the `--config` option in order to change your Jest config file, and this is useful when you have a monorepo which contains Frontend and Backend code, so you can configure Jest for working in both types o project.
- The previous mechanism could be a mess, so you can get the same behavior by doing the following:
    - Create a `jest.config.js` and add your common configuration
      ```js
      module.exports = {
        moduleDirectories: [
          'node_modules',
          'shared',
        ],
        coverageThreshold: {
          global: {
            statements: 15,
            branches: 10,
            functions: 15,
            lines: 15,
          },
        },
        projects: [
          './client',
          './server',
        ],
      }
      ```
    - Create a `jest.server.js` and add your server configuration
      ```js
      module.exports = {
        displayName: 'server',
        testEnvironment: 'jest-environment-node',
        testMatch: ['**/server/**/*.js'],
      }
      ```
    - Create a `jest.client.js` and add your server configuration
      ```js
      module.exports = {
        displayName: 'client',
        testEnvironment: 'jest-environment-jsdom',
        setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
        snapshotSerializers: ['@emotion/jest/serializer'],
      }
      ```
    - Now you can execute your test as you do normally
- You can check your final Jest config file by executing
  ```console
  npx jest --showConfig
  ```
- If you want to run Eslint with Jest, you have to do the following:
  - Install a package called `jest-runner-eslint`
  - Create a Jest config file for Eslint, e.g. `jest.eslint.js`
    ```js
    module.exports = {
      displayName: 'lint',
      runner: 'jest-runner-eslint',
      testMatch: ['<rootDir>/**/*.js'],
    }
    ```
  - Add the previous config to your main Jest config file
  - Now you can run Eslint with by using the jest command
- If you want to run your tests by previously selecting your project (when you have many Jest config files), so, you have to do the following
  - Install `jest-watch-select-projects`
  - Thanks to watch mode is plugable, you can install and config some plugins by using the `watchPlugins` property
    ```js
    {
      watchPlugins: [
        'jest-watch-select-projects',
      ],
    }
    ```
- If you want that Jest suggest you a test to run when you want to execute a specific test file in watch mode, so, you have to do the following
  - Install `jest-watch-typeahead`
  - Thanks to watch mode is plugable, you can install and config some plugins by using the `watchPlugins` property
    ```js
    {
      watchPlugins: [
        'jest-watch-typeahead/filename',
        'jest-watch-typeahead/testname',
      ],
    }
    ```
- If you want to run tests that are related to the files that you have changed, you can add the following option to jest command `jest --findRelatedTests`

### 6. React testing library course
A package which contains code tha uses `react-testing-library` for testing React applications

You can run executing the following sentence:
```
npm run -w react-testing-library-course test
```

##### Hints
- When testing, code that causes React state updates should be wrapped into `act(...)`, this function is exposed by `react-testing-library`
  - The `waitFor, waitForElementToBeRemoved, userEvent, fireEvent and findBy*` use the `act(...)` behind the scene
  - But if you want to test a custom hook which updates the state of a React component, you need to surround that into an `act(...)` function
- React Testing Library has an utility for testing hooks, which is called `renderHook` and it returns a `rerender` method too
- React Testing Library has an `unmount` method, which works for unmounting a component, in that way you can execute the function returned by the `useEffect` hook
- It is better to use `findBy*` in place of `getBy*`, because if you add some async code and you are using `getBy*` it will brake your code
- It is better to use `userEvent` instead of `fireEvent`, because first one is more realistic, e.g, when you type text on an input, many events are triggered (key down, key up, etc), and `userEvent` replicates all of them 

### 7. Cypress test
A package which contains code that uses `Cypress` for making end to end tests

You can run executing the following sentence:
```
npm run -w cypress-test cy:open
```

##### Hints
- There is a Eslint plugin for Cypress called [eslint-plugin-cypress](https://www.npmjs.com/package/eslint-plugin-cypress)
- There is a library called [@testing-library/cypress]([@testing-library/cypress](https://testing-library.com/docs/cypress-testing-library/intro/)) which extends the selectors for Cypress
- If you want to run your web application and Cypress with one command you can use [start-server-and-test](https://www.npmjs.com/package/start-server-and-test), which waits until your web page is running in order to execute your Cypress tests
- We have many additional useful things for debugging or web app with Cypress
  - First we can debug by using the `debugger` keyword
  - We have a variable to know if our app is being run by Cypress `window.Cypress`
  - When you do a query with `get` or `findBy*` (using @testing-library/cypress), you can use `then`, and it receive a `subject` object which you have to return in order to no broke your test, but id addition you can put a `debugger` in the `then` block
  - When you do a query with `get` or `findBy*` you can use the `debug` or `pause` method
- You can create reusable chunks of code by using the `Cypress.Commands.add(name, ...parameters)`
- You can mock requests by using `Cypress.route(...)`
  ```javascript
  cy.route({
    method: 'POST',
    url: 'http://localhost:3000/register',
    status: 500,
    response: {},
  })
  ```
- You can do request by using `Cypress.request(...)`
  ```javascript
  cy.request({
    url: 'http://localhost:3000/login',
    method: 'POST',
    body: {},
  })
  ```
- You can install React Developer Tools inside of Cypress, for that you have to install the chrome extension and add this chunk of code at the beginning of your `index.js` file or in your `index.html`
  ```javascript
  if (window.Cypress) {
    window.__REACT_DEVTOOLS_GLOBAL_HOOK__ =
      window.parent.__REACT_DEVTOOLS_GLOBAL_HOOK__
  }
  ```
  This will load your app up to React Dev Tools, if you do not add this, you will not see your app in React Dev Tools

### 8. Node test
A package which contains code that uses `Jest` for testing a Node app

You can run executing the following sentence:
```
npm run -w node-testing test:exercise:watch
```

##### Hints
- You can use [Jest In Case](https://github.com/atlassian/jest-in-case) library in order to create more descriptive tests, but you can get the same behavior by using `it.each`
- When you have complex error object and you update it, it is recommended to use `toMatchInlineSnapshot` instead of `toHaveBeenCalledWith`, e.g.
  ```javascript
  expect(res.json).toHaveBeenCalledWith(hugeErrorObjectUpdatedManually)
  expect(res.json.mock.calls[0])toMatchInlineSnapshot(hugeErrorObjectUpdatedByJest)
  ```

### 9. Test Containers
A package which contains code that uses [Test Containers](https://testcontainers.com) for creating unit and integration tests

- First you have to create an `.env` file by using the `.env.example`
- Then you can run executing the following sentence:
```
npm run -w test-containers test
```

##### Hints
- You need to run this image locally if you want to use [Prisma](https://www.prisma.io)
  ```bash
  docker run -d -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=password -e MONGO_INITDB_DATABASE=todo -p 27017:27017 prismagraphql/mongo-single-replica:5.0.3
  ```
  - In addition you have to use this url connection
  ```bash
  DATABASE_URL=mongodb://root:password@localhost:27017/todo?authSource=admin
  ```
  

### Useful content
- [Kent C. Dodds site](https://kentcdodds.com), who is the creator of Testing Library
- [Common mistakes](https://kentcdodds.com/blog/common-mistakes-with-react-testing-library) with React Testing Library
- [Useful Playground](https://testing-playground.com) that you can open by using the `screen.logTestingPlaygroundURL()` method
- [Useful Chrome extensions](https://chrome.google.com/webstore/detail/testing-playground/hejbmebodbijjdhflfknehhcgaklhano/related) for finding the best type of query
- [Priority of queries](https://testing-library.com/docs/queries/about#priority)
- [Info](https://www.w3.org/TR/accname-1.1/) about accessible name and description computation
- [Useful info](https://developer.chrome.com/blog/full-accessibility-tree/) about how to run manual test for accessibility
- Request interceptors for testing:
  - [MSW](https://mswjs.io)
  - [Nock](https://github.com/nock/nock)
- [Test Data Bot](https://www.npmjs.com/package/@jackfranklin/test-data-bot) generates fake yet realistic looking data for your unit tests. Rather than creating random objects each time you want to test something in your system you can instead use a factory that can create fake data. This keeps your tests consistent and means that they always use data that replicates the real thing. If your tests work off objects close to the real thing they are more useful and there's a higher chance of them finding bugs.
- You can use [fake timers with Jest](https://jestjs.io/docs/timer-mocks), this is useful for cases where you want to test code inside of `setInterval` or `setTimeout`
- You can use [Jest When](https://github.com/timkindberg/jest-when#readme) library in order to specify dynamic return values for specifically matched mocked function arguments
- You can use [expect.any(String|Number|Boolean)](https://jestjs.io/docs/en/expect#expectanyconstructor) with `toEqual or toBeCalledWith` as follow
  ```javascript
  expect(data).toEqual({
    user: {
      username: user.username,
      token: expect.any(String),
      id: expect.any(String),
    },
  })
  ```
- You can use [Clean Stack](https://github.com/sindresorhus/clean-stack) library for having better error in your tests
- It is a good idea to bootstrap servers by using the `JEST_WORKER_ID`, in that way we can run our test in parallel because ports are dynamic, for instance
  ```javascript
  const port = 8000 + Number(process.env.JEST_WORKER_ID)
  process.env.PORT = process.env.PORT || port
  ```
- If you want more matchers, you can install [Jest Extended](https://github.com/jest-community/jest-extended)
- You can use the `DEBUG_PRINT_LIMIT` env var for increasing or decreasing the lines to display by the console, or you can use the second parameter of `debug` method, for example:
  ```javascript
  debug(undefined, 1000)
  debug(undefined, Infinity)
  ```
- [Jest Preview](https://www.jest-preview.com) is a nice tool for displaying your UI when a test is running
- The `--silent` parameter is another way of silent `console.log
- [Testing Library Table Queries](https://github.com/lexanth/testing-library-table-queries#readme) has additional queries for working with tables
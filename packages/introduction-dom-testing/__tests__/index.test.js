import  { getQueriesForElement, fireEvent, prettyDOM } from '@testing-library/dom';

const getDOM = () => {
  document.body.innerHTML = `
  <div>
    <button id='increment'>+</button>
    <button id='decrement'>-</button>
    <span id='result'>0</span>
  </div>
  `;


  const increment = document.getElementById('increment');
  const decrement = document.getElementById('decrement');
  const result = document.getElementById('result');
  
  increment.addEventListener('click', () => {
    result.innerHTML = Number(result.innerHTML) + 1;
  });
  
  decrement.addEventListener('click', () => {
    result.innerHTML = Number(result.innerHTML) - 1;
  });

  return document.body;
};

describe('Counter', () => {
  afterEach(() => {
    console.log(prettyDOM());
  });

  it('should render the counter', () => {
    const container = getDOM();
    const { getByText } = getQueriesForElement(container);
    
    expect(getByText('+')).toBeVisible();
    expect(getByText('-')).toBeVisible();
    expect(getByText('0')).toBeVisible();
  });

  it('should increase the counter', () => {
    const container = getDOM();
    const { getByText } = getQueriesForElement(container);
    
    fireEvent.click(getByText('+'));
    
    expect(getByText('1')).toBeVisible();
  });

  it('should decrease the counter', () => {
    const container = getDOM();
    const { getByText } = getQueriesForElement(container);
    
    fireEvent.click(getByText('-'));
    
    expect(getByText('-1')).toBeVisible();
  });

  it('should compare against a snapshot', () => {
    const container = getDOM();
    expect(container).toMatchSnapshot();
  });

  it('should compare against an inline snapshot', () => {
    const container = getDOM();
    expect(container).toMatchInlineSnapshot(`
      <body>
        
        
        <div>
          
          
          <button
            id="increment"
          >
            +
          </button>
          
          
          <button
            id="decrement"
          >
            -
          </button>
          
          
          <span
            id="result"
          >
            0
          </span>
          
        
        </div>
        
        
      </body>
    `);
  });
});
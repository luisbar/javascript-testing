mock('./game/getWinner', () => ({
  getWinner: () => 'player 1',
}));

describe('Player module', () => {
  test('should always get the player 1', () => {
    const { getWinner } = require('./game/getWinner');
    expect(getWinner()).toBe('player 1');
  });

  test('should always get the player 2', () => {
    const { restoreMock } = require('./game/getWinner');
    restoreMock();
    const { getWinner } = require('./game/getWinner');
    expect(getWinner()).toBe('player 2');
  });

  test('should get a board of 3x3', () => {
    const getBoard = require('./game/getBoard')
    const req = {
      size: 9,
    };
    const res = {
      json: fn((value) => value),
    };

    getBoard(req, res);
    
    expect(res.json).toHaveBeenCalledWith({
      board: [0,0,0,0,0,0,0,0,0],
    });
    expect(res.json.mock.calls.length).toBe(1);
  });
});
function getBoard(req, res) {
  const size = req.size;

  res.json({
    board: Array.from({ length: size }).fill(0),
  });
}

module.exports = getBoard;
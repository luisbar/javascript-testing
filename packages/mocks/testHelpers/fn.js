function fn(mockProcucer) {
  function mockWrapper() {
    mockWrapper.mock.calls.push(...arguments);
    return mockProcucer(...arguments);
  }

  mockWrapper.mock = { calls: [] };

  return mockWrapper;
}

module.exports = fn;
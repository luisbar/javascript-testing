const { Module } = require('module');
const path = require('path');

function getCallerFolderPath() {
  return module.parent.path
  .split('/')
  .reverse()
  .slice(1)
  .reverse()
  .join('/');
}

function getAbsolutePathOfModuleToMock({ relativeModulePath }) {
  return path
  .resolve(getCallerFolderPath(), relativeModulePath);
}

function getOriginalModuleFromCache({ absoluteModulePath }) {
  return require.cache[`${absoluteModulePath}.js`];
}

function getMockImplementation({
  mockProcucer,
  absoluteModulePath,
}) {
  require(absoluteModulePath);
  const originalModule = getOriginalModuleFromCache({ absoluteModulePath });

  return {
    ...mockProcucer(),
    restoreMock: function() {
      require.cache[`${absoluteModulePath}.js`] = originalModule;
    },
  }
}

function loadMockedModule({ absoluteModulePath, mockImplementation }) {
  const mockedModule = new Module(`${absoluteModulePath}.js`);
  mockedModule.exports = mockImplementation;
  require.cache[`${absoluteModulePath}.js`] = mockedModule;
}

function mock(relativeModulePath, mockProcucer) {
  const absoluteModulePath = getAbsolutePathOfModuleToMock({ relativeModulePath });
  const mockImplementation = getMockImplementation({ mockProcucer, absoluteModulePath });
  loadMockedModule({
    absoluteModulePath,
    mockImplementation,
  });
}

module.exports = mock;
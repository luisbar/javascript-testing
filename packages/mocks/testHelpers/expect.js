const baseExpect = require('basis/testHelpers/expect');

function expect(actual) {
  return {
    ...baseExpect.apply(this, arguments),
    toHaveBeenCalledWith: function (expected) {
      if (!actual.mock.calls.find((callData) => JSON.stringify(callData) === JSON.stringify(expected)))
        throw new Error (`${actual.constructor.name} has not been called with ${JSON.stringify(expected)}`);
    }
  }
}

module.exports = expect;
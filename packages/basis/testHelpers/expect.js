function expect(actual) {
  return {
    toBe: (expected) => {
      if (actual !== expected) {
        throw new Error (`${actual} is not equal ${expected}`);
      }
    },
  };
}

module.exports = expect;
async function test(message, callback) {
  try {
    await callback();
    console.log(` ✓ ${message}`);
  } catch (error) {
    console.error(` ✗ ${message}`);
    console.error(`  ${error}`);
  }
}

module.exports = test;
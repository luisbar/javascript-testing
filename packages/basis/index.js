const math = require('./math');
const { add, addAsync } = math.add;
const { substract, substractAsync } = math.substract;

describe('math module', () => {
  test('should add two values', () => {
    expect(add(1, 2)).toBe(3);
  });

  test('should addAsync two values', async () => {
    expect(await addAsync(1, 2)).toBe(3);
  });
  
  test('should substract two values', () => {
    expect(substract(3, 2)).toBe(1);
  });

  test('should substractAsync two values', async () => {
    expect(await substractAsync(3, 2)).toBe(1);
  });
});
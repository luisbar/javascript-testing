function add(a, b) {
  return a + b;
}

async function addAsync() {
  return add(...arguments);
}

module.exports = {
  add,
  addAsync,
};

function substract(a, b) {
  return a - b;
}

async function substractAsync() {
  return substract(...arguments);
}

module.exports = {
  substract,
  substractAsync,
};

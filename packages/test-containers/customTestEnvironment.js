const { TestEnvironment } = require('jest-environment-node')

class CustomTestEnvironment extends TestEnvironment {
  constructor(config, context) {
    super(config, context)
    this.testPath = context.testPath
  }

  async setup() {
    await super.setup()

    this.global.testFilePath = this.testPath
  }

  async teardown() {
    await super.teardown()
  }

  getVmContext() {
    return super.getVmContext()
  }
}

module.exports = CustomTestEnvironment

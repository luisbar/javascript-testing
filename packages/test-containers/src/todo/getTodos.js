const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

const getTodos = async (req, res) => {
  const todos = await prisma.todo.findMany()
  res.send(todos)
}

module.exports = getTodos
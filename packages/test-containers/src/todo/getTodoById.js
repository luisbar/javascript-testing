const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

const getTodoById = async (req, res) => {
  const todo = await prisma.todo.findUnique({
    where: {
      id: req.params.id
    }
  })
  res.send(todo)
}

module.exports = getTodoById
const { PrismaClient } = require('@prisma/client')
const { ObjectId } = require('bson')
const prisma = new PrismaClient()

const createTodo = async (req, res) => {
  try {
  
    const todo = await prisma.todo.create({ data: {
      ...req.body,
      id: new ObjectId(),
    } })
  
    res.send(todo)
  
  } catch (error) {
    
    if (error.code === 'P2002')
      throw new Error('Duplicated name')
    
    throw new Error('Error creating todo')
  }
}

module.exports = createTodo
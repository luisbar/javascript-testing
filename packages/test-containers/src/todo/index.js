const fs = require('fs')
const excludedFiles = ['index', 'createTodo.scenarios', 'createTodo.test']

const exportModulesInTheCurrentLocation = () => {
  fs.readdirSync(`${__dirname}`).forEach((file) => {
    const moduleName = file.replace('.js', '')
    if (file.match(/\.js$/) !== null && !excludedFiles.includes(moduleName)) {
      exports[moduleName] = require(`./${moduleName}`)
    }
  })
}

exportModulesInTheCurrentLocation()

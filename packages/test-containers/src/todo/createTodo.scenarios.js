const { ObjectId } = require('bson')

exports.duplicatedName = {
  todo: {
    one: { data: { id: new ObjectId(), name: 'todo 1', description: 'todo 1 description'  } },
  }
}
const scenarios = require('./createTodo.scenarios')
const createTodo = require('./createTodo')

describe('createTodo', () => {
  let req, res, todo

  beforeEach(() => {
    todo = {
      name: 'todo success',
      description: 'todo description'
    }
    req = {
      body: todo,
    }
    res = {
      send: jest.fn(),
    }
  })

  scenario('it should create a todo', async () => {
    await createTodo(req, res)

    expect(res.send).toHaveBeenCalledWith(expect.objectContaining(todo))
    expect(res.send).toHaveBeenCalledWith(expect.objectContaining({
      id: expect.any(String),
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
      enabled: true,
    }))
  })

  scenario('duplicatedName', 'it should fail to create a todo with a duplicate name', async () => {
    req.body = {
      ...scenarios.duplicatedName.todo.one.data
    }
    await expect(createTodo(req, res)).rejects.toThrow('Duplicated name')
  })
})
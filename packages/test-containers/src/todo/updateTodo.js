const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

const updateTodo = async (req, res) => {
  const todo = await prisma.todo.update({
    where: {
      id: req.params.id
    },
    data: req.body
  })
  res.send(todo)
}

module.exports = updateTodo
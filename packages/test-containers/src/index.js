const express = require('express')
const app = express()
const todoControllers = require('./todo')

app.use(express.json())

app.get('/', todoControllers.getTodos)
app.get('/:id', todoControllers.getTodoById)
app.post('/', todoControllers.createTodo)
app.put('/:id', todoControllers.updateTodo)

const listener = app.listen(0, (a) => {
  console.log(`http://localhost:${listener.address().port}`);
})

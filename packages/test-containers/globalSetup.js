// Run before all tests
const { GenericContainer } = require('testcontainers');
const { expand } = require('dotenv-expand');
const dotenv = require('dotenv-safe').config();

expand(dotenv);

module.exports = async function (globalConfig, projectConfig) {
  const image = await GenericContainer
  .fromDockerfile('.')
  .build()
  .then(image => {
    console.log('MySQL image built');
    return image
  });

  global.container = await image
  .withEnvironment({
    MYSQL_ROOT_PASSWORD: process.env.MYSQL_PASSWORD,
    MYSQL_DATABASE: process.env.MYSQL_DATABASE,
  })
  .withExposedPorts(3306)
  .start()
  .then(container => {
    console.log('MySQL container started');
    return container
  });

  process.env.DATABASE_URL = `mysql://${process.env.MYSQL_USER}:${process.env.MYSQL_PASSWORD}@${global.container.getHost()}:${global.container.getMappedPort(3306)}/${process.env.MYSQL_DATABASE}`;
};
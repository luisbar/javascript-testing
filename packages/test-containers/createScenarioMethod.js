// Run before each test and it has Jest globals vars
const path = require('path')
const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

const buildScenario = (it, testFilePath) => (...args) => {
  if (args.length < 2)
    throw new Error('buildScenario expects at least 2 arguments: testName and testFunction')

  const [scenarioName, testName, testFunction] = args.length === 2 ? [undefined, ...args] : args

  return it(testName, async () => {
    const parsedPath = path.parse(testFilePath)
    const testFileNameParts = parsedPath.name.split('.')
    const scenarioFilePath = `${parsedPath.dir}/${testFileNameParts
    .slice(0, testFileNameParts.length - 1)
    .join('.')}.scenarios`

    const allScenarios = require(scenarioFilePath)
    const scenario = allScenarios[scenarioName]

    if (!scenario)
      console.warn(`Test "${testName}" in ${JSON.stringify(parsedPath.dir)} doesn't have a scenario`);

    const scenarioData = await seedScenario(scenario)

    await testFunction(scenarioData)
  })
}

const seedScenario = async (scenario) => {
  if (!scenario) return {}

  const scenarios = {}
  for (const [model, namedFixtures] of Object.entries(scenario)) {
    scenarios[model] = {}
    for (const [name, createArgs] of Object.entries(namedFixtures)) {
        scenarios[model][name] = await prisma[model].create(
          createArgs
        )
    }
  }
  return scenarios
}

global.scenario = buildScenario(global.it, global.testFilePath)
module.exports = {
  displayName: 'test-containers',
  roots: ['<rootDir>/src'],
  rootDir: __dirname,
  testEnvironment: 'node',
  setupFilesAfterEnv: ['<rootDir>/createScenarioMethod.js'],
  globalSetup: '<rootDir>/globalSetup.js',
  globalTeardown: '<rootDir>/globalTeardown.js',
  testEnvironment: '<rootDir>/customTestEnvironment.js',
}

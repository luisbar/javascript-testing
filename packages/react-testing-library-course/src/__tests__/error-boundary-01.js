import * as React from 'react'
import {render} from '@testing-library/react'
import {reportError as mockReportError} from '../api'
import {ErrorBoundary} from '../error-boundary'
import userEvent from '@testing-library/user-event'

jest.mock('../api')

// this is only here to make the error output not appear in the project's output
// even though in the course we don't include this bit and leave it in it's incomplete state.
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => {})
})

afterEach(() => {
  console.error.mockRestore()
})

afterAll(() => {
  jest.clearAllMocks()
})

function Bomb({shouldThrow}) {
  if (shouldThrow)
    throw new Error('💣')
  
  return <div>Success</div>
}

test('calls reportError and renders that there was a problem', () => {
  mockReportError.mockResolvedValueOnce({success: true})
  const {rerender, getByRole, queryByRole, getByText} = render(
    <ErrorBoundary>
      <Bomb />
    </ErrorBoundary>,
  )

  rerender(
    <ErrorBoundary>
      <Bomb shouldThrow={true} />
    </ErrorBoundary>,
  )

  expect(mockReportError).toHaveBeenCalledTimes(1)
  expect(getByRole('alert')).toHaveTextContent('There was a problem.')
  mockReportError.mockClear()

  rerender(
    <ErrorBoundary>
      <Bomb/>
    </ErrorBoundary>,
  )

  expect(mockReportError).not.toHaveBeenCalled()
  expect(queryByRole('button')).toHaveTextContent('Try again?')

  userEvent.click(getByRole('button'))

  expect(queryByRole('alert')).not.toBeInTheDocument()
  expect(getByText('Success')).toBeInTheDocument()
})
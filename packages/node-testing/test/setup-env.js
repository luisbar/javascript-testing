import * as matchers from 'jest-extended';
expect.extend(matchers);

const port = 8000 + Number(process.env.JEST_WORKER_ID)
process.env.PORT = process.env.PORT || port

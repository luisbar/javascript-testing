import axios from 'axios'
import {resetDb, insertTestUser} from 'utils/db-utils'
import {getData, handleRequestFailure, resolve} from 'utils/async'
import * as generate from 'utils/generate'
import * as booksDB from '../db/books'
import startServer from '../start'

let baseURL, server

beforeAll(async () => {
  server = await startServer()
  baseURL = `http://localhost:${server.address().port}/api`
})

afterAll(() => server.close())

beforeEach(() => resetDb())

async function setup() {
  const testUser = await insertTestUser()
  const authAPI = axios.create({baseURL})
  authAPI.defaults.headers.common.authorization = `Bearer ${testUser.token}`
  authAPI.interceptors.response.use(getData, handleRequestFailure)
  return {testUser, authAPI}
}

test('listItem CRUD', async () => {
  const {testUser, authAPI} = await setup()
  const book = generate.buildBook()
  booksDB.insert(book)

  const createdData = await authAPI.post(
    `/list-items`,
    {
      bookId: book.id,
    },
    {
      headers: {
        authorization: `Bearer ${testUser.token}`,
      },
    },
  )

  expect(createdData).toMatchObject({
    listItem: {
      book: {
        author: expect.any(String),
        coverImageUrl: expect.any(String),
        id: book.id,
        pageCount: expect.any(Number),
        publisher: expect.any(String),
        synopsis: expect.any(String),
        title: expect.any(String),
      },
      bookId: book.id,
      id: expect.any(String),
      notes: expect.any(String),
      ownerId: testUser.id,
      rating: expect.any(Number),
      startDate: expect.any(Number || null),
    },
  })
  expect(createdData.listItem.finishDate).toBeOneOf([expect.any(String), null])

  const readData = await authAPI.get(`/list-items/${createdData.listItem.id}`, {
    headers: {
      authorization: `Bearer ${testUser.token}`,
    },
  })

  expect(readData).toMatchObject({
    listItem: {
      book: {
        author: expect.any(String),
        coverImageUrl: expect.any(String),
        id: book.id,
        pageCount: expect.any(Number),
        publisher: expect.any(String),
        synopsis: expect.any(String),
        title: expect.any(String),
      },
      bookId: book.id,
      id: expect.any(String),
      notes: expect.any(String),
      ownerId: testUser.id,
      rating: expect.any(Number),
      startDate: expect.any(Number || null),
    },
  })
  expect(readData.listItem.finishDate).toBeOneOf([expect.any(String), null])

  const updatedData = await authAPI.put(
    `/list-items/${createdData.listItem.id}`,
    {
      notes: generate.notes(),
    },
    {
      headers: {
        authorization: `Bearer ${testUser.token}`,
      },
    },
  )

  expect(updatedData).toMatchObject({
    listItem: {
      book: {
        author: expect.any(String),
        coverImageUrl: expect.any(String),
        id: book.id,
        pageCount: expect.any(Number),
        publisher: expect.any(String),
        synopsis: expect.any(String),
        title: expect.any(String),
      },
      bookId: book.id,
      // finishDate: expect.any(null), TODO: try with toBeOneOf of jest-extended
      id: expect.any(String),
      notes: expect.any(String),
      ownerId: testUser.id,
      rating: expect.any(Number),
      startDate: expect.any(Number || null),
    },
  })
  expect(updatedData.listItem.finishDate).toBeOneOf([expect.any(String), null])


  const deletedData = await authAPI.delete(
    `/list-items/${createdData.listItem.id}`,
    {
      headers: {
        authorization: `Bearer ${testUser.token}`,
      },
    },
  )

  expect(deletedData).toStrictEqual({
    success: true,
  })

  const notFoundData = await authAPI
    .get(`/list-items/${createdData.listItem.id}`, {
      headers: {
        authorization: `Bearer ${testUser.token}`,
      },
    })
    .catch(resolve)

  expect(notFoundData).toMatchInlineSnapshot(
    `[Error: 404: {"message":"No list item was found with the id of ${createdData.listItem.id}"}]`,
  )
})

/* eslint no-unused-vars:0 */

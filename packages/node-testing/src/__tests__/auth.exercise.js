import axios from 'axios'
import {handleRequestFailure, getData, resolve} from 'utils/async'
import {resetDb} from 'utils/db-utils'
import * as generate from 'utils/generate'
import startServer from '../start'
import * as usersDb from '../db/users'

describe('auth-controller', () => {
  let server, requestor
  const user = generate.loginForm()

  beforeAll(async () => {
    server = await startServer()
    requestor = axios.create({
      baseURL: `http://localhost:${server.address().port}/api`,
    })
    requestor.interceptors.response.use(getData, handleRequestFailure)
  })

  afterAll(async () => {
    await server.close()
  })

  beforeEach(async () => {
    await resetDb()
  })

  it('should register a new user', async () => {
    const data = await requestor.post('auth/register', user)

    expect(data).toEqual({
      user: {
        username: user.username,
        token: expect.any(String),
        id: expect.any(String),
      },
    })
  })

  it('should throw when username is taken', async () => {
    usersDb.insert(user)
    const data = await requestor.post('auth/register', user).catch(resolve)

    expect(data).toMatchInlineSnapshot(
      `[Error: 400: {"message":"username taken"}]`,
    )
  })

  it('should login a user', async () => {
    await requestor.post('auth/register', user)
    const data = await requestor.post('auth/login', user)

    expect(data).toEqual({
      user: {
        username: user.username,
        token: expect.any(String),
        id: expect.any(String),
      },
    })
  })

  it('should get a user', async () => {
    const {
      user: {token},
    } = await requestor.post('auth/register', user)
    const data = await requestor.get('auth/me', {
      headers: {Authorization: `Bearer ${token}`},
    })

    expect(data).toEqual({
      user: {
        username: user.username,
        token: expect.any(String),
        id: expect.any(String),
      },
    })
  })
})

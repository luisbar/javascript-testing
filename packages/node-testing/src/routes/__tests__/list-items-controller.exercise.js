jest.mock('../../db/books')
jest.mock('../../db/list-items')
import * as generate from 'utils/generate'
import * as booksDB from '../../db/books'
import * as listItemsDB from '../../db/list-items'
import * as listItemsController from '../list-items-controller'

describe('list-items-controller', () => {

  describe('setListItem', () => {
    const user = generate.buildUser()
    const listItem = generate.buildListItem({ owner: user })
    const req = generate.buildReq({user, params: {id: listItem.id}})
    const res = generate.buildRes()
    const next = generate.buildNext()

    beforeEach(() => {
      jest.clearAllMocks()
    })

    afterAll(() => {
      jest.resetAllMocks()
    })

    it('should return a 404 if the list item does not exist', async () => {
      listItemsDB.readById.mockResolvedValueOnce(null)
      await listItemsController.setListItem(req, res, next)
      
      expect(res.status).toHaveBeenNthCalledWith(1, 404)
      expect(res.json).toHaveBeenNthCalledWith(1, {message: `No list item was found with the id of ${listItem.id}`})
      expect(next).not.toHaveBeenCalled()
    })

    it('should return a 403 if the user is not the owner of the list item', async () => {
      listItemsDB.readById.mockResolvedValueOnce({ ...listItem, ownerId: generate.buildUser().id })
      await listItemsController.setListItem(req, res, next)

      expect(res.status).toHaveBeenNthCalledWith(1, 403)
      expect(res.json).toHaveBeenNthCalledWith(1, {
        message: `User with id ${user.id} is not authorized to access the list item ${listItem.id}`,
      })
      expect(next).not.toHaveBeenCalled()
    })
    
    it('should add all the list item properties to the request', async () => {
      listItemsDB.readById.mockResolvedValue(listItem)
      await listItemsController.setListItem(req, res, next)

      expect(req.listItem).toEqual(listItem)
      expect(next).toHaveBeenCalled()
      expect(res.status).not.toHaveBeenCalled()
      expect(res.json).not.toHaveBeenCalled()
    })
  })

  describe('getListItems', () => {
    const books = Array.from({length: 3}, generate.buildBook)
    const user = generate.buildUser()
    const listItems = books.map(book => generate.buildListItem({ owner: user, book }))
    const req = generate.buildReq({user})
    const res = generate.buildRes()
    
    beforeAll(() => {
      listItemsDB.query.mockResolvedValue(listItems)
      booksDB.readManyById.mockResolvedValue(books)
    })

    afterAll(() => {
      jest.resetAllMocks()
    })

    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('should return a list of items', async () => {
      await listItemsController.getListItems(req, res)

      expect(listItemsDB.query).toHaveBeenNthCalledWith(1, {ownerId: req.user.id})
      expect(booksDB.readManyById).toHaveBeenNthCalledWith(1, listItems.map(li => li.bookId))
      expect(res.json).toHaveBeenCalledWith({ listItems })
    })
  })

  describe('getListItem', () => {
    const book = generate.buildBook()
    const listItem = generate.buildListItem({ book })
    const req = generate.buildReq({listItem})
    const res = generate.buildRes()
    
    beforeAll(async () => {
      booksDB.readById.mockResolvedValue(book)
    })

    afterAll(() => {
      jest.resetAllMocks()
    })

    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('should return a single item', async () => {
      await listItemsController.getListItem(req, res)

      expect(booksDB.readById).toHaveBeenNthCalledWith(1, listItem.bookId)
      expect(res.json).toHaveBeenCalledWith({ listItem })
    })
  })

  describe('createListItem', () => {
    const book = generate.buildBook()
    const user = generate.buildUser()
    const listItem = generate.buildListItem({ book, owner: user })
    let req
    let res
    
    beforeAll(() => {
      req = generate.buildReq({user, body: {bookId: book.id}})
      res = generate.buildRes()
      listItemsDB.query.mockResolvedValue([])
      listItemsDB.create.mockResolvedValue(listItem)
      booksDB.readById.mockResolvedValue(book)
    })

    afterAll(() => {
      jest.resetAllMocks()
    })

    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('should create a new list item', async () => {
      await listItemsController.createListItem(req, res)

      expect(listItemsDB.query).toHaveBeenNthCalledWith(1, {ownerId: user.id, bookId: book.id})
      expect(listItemsDB.create).toHaveBeenNthCalledWith(1, { ownerId: user.id, bookId: book.id })
      expect(booksDB.readById).toHaveBeenNthCalledWith(1, book.id)
      expect(res.json).toHaveBeenNthCalledWith(1, { listItem })
      expect(res.status).not.toHaveBeenCalled()
    })

    it('should return an error if the bookId is not sent', async () => {
      req.body = {}
      await listItemsController.createListItem(req, res)

      expect(res.status).toHaveBeenNthCalledWith(1, 400)
      expect(res.json).toHaveBeenNthCalledWith(1, {
        message: `No bookId provided`
      })
      expect(listItemsDB.query).not.toHaveBeenCalled()
      expect(listItemsDB.create).not.toHaveBeenCalled()
    })

    it('should return an error if the user has a list item for the book specified', async () => {
      req = generate.buildReq({user, body: {bookId: book.id}})
      listItemsDB.query.mockResolvedValue([listItem])
      await listItemsController.createListItem(req, res)

      expect(listItemsDB.query).toHaveBeenNthCalledWith(1, {ownerId: user.id, bookId: book.id})
      expect(res.status).toHaveBeenNthCalledWith(1, 400)
      expect(res.json).toHaveBeenNthCalledWith(1, {
        message: `User ${user.id} already has a list item for the book with the ID ${book.id}`,
      })
      expect(listItemsDB.create).not.toHaveBeenCalled()
    })
  })

  describe('updateListItem', () => {
    const book = generate.buildBook()
    const user = generate.buildUser()
    const listItem = generate.buildListItem({ book, owner: user })
    const body = {}
    const req = generate.buildReq({listItem, body})
    const res = generate.buildRes()
    
    beforeAll(() => {
      listItemsDB.update.mockResolvedValue(listItem)
      booksDB.readById.mockResolvedValue(book)
    })

    afterAll(() => {
      jest.resetAllMocks()
    })

    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('should update the list item', async () => {
      await listItemsController.updateListItem(req, res)

      expect(listItemsDB.update).toHaveBeenNthCalledWith(1, listItem.id, body)
      expect(booksDB.readById).toHaveBeenNthCalledWith(1, book.id)
      expect(res.json).toHaveBeenCalledWith({ listItem })
    })
  })

  describe('deleteListItem', () => {
    const req = generate.buildReq({ listItem: generate.buildListItem() })
    const res = generate.buildRes()

    afterAll(() => {
      jest.resetAllMocks()
    })

    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('should delete the list item', async () => {
      await listItemsController.deleteListItem(req, res)

      expect(listItemsDB.remove).toHaveBeenNthCalledWith(1, req.listItem.id)
      expect(res.json).toHaveBeenNthCalledWith(1, { success: true })
    })
  })
})

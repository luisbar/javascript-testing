// Testing Pure Functions
import cases from 'jest-in-case'
import {isPasswordAllowed} from '../auth'


describe('isPasswordAllowed', () => {
  
  it.each([
    ['!aBc123'],
  ])
  ('should return true for valid passwords', (password) => {
    expect(isPasswordAllowed(password)).toBe(true)
  })

  it.each([
    ['a2c!'],
    ['123456!'],
    ['ABCdef!'],
    ['abc123!'],
    ['ABC123!'],
    ['ABCdef123'],
  ])
  ('should return false for invalid password %s', (password) => {
    expect(isPasswordAllowed(password)).toBe(false)
  })

})

cases(
  'isPasswordAllowed valid password',
  (options) => {
    expect(isPasswordAllowed(options.password)).toBe(true)
  },
  {
    'valid password': {
      password: '!aBc123',
    },
  }
)

cases(
  'isPasswordAllowed invalid password',
  (options) => {
    expect(isPasswordAllowed(options.password)).toBe(false)
  },
  {
    'too short password': {
      password: '!aBc',
    },
    'no letters in password': {
      password: '123456!',
    },
    'no digits in password': {
      password: 'ABCdef!',
    },
    'no capital letters in password': {
      password: 'abc123!',
    },
    'no lowercase letters in password': {
      password: 'ABC123!',
    },
    'no special characters in password': {
      password: 'ABCdef123',
    },
  }
)

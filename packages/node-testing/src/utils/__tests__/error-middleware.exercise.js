// Testing Middleware

import {UnauthorizedError} from 'express-jwt'
import { buildRes, buildNext, buildReq } from 'utils/generate'
import errorMiddleware from '../error-middleware'

describe('errorMiddleware', () => {
  let next, req, res
  const UNAUTHORIZED_ERROR = new UnauthorizedError('some_error_code', {message: 'Some message'})
  const NON_UNAUTHORIZED_ERROR = new Error('Some other error')

  beforeEach(() => {
    next = buildNext()
    req = buildReq()
    res = buildRes()
  })

  it('should call next with unauthorized error', () => {
    res = buildRes({headersSent: true})
    errorMiddleware(UNAUTHORIZED_ERROR, req, res, next)

    expect(next).toHaveBeenCalledWith(UNAUTHORIZED_ERROR)
    expect(next).toHaveBeenCalledTimes(1)
    expect(res.status).not.toHaveBeenCalled()
    expect(res.json).not.toHaveBeenCalled()
  })

  it('should call res.status with 401 if error is instance of UnauthorizedError', () => {
    errorMiddleware(UNAUTHORIZED_ERROR, req, res, next)

    expect(next).not.toHaveBeenCalled()
    expect(res.status).toHaveBeenCalledWith(401)
    expect(res.status).toHaveBeenCalledTimes(1)
    expect(res.json).toHaveBeenCalledWith({ code: UNAUTHORIZED_ERROR.code, message: UNAUTHORIZED_ERROR.message})
    expect(res.json).toHaveBeenCalledTimes(1)
  })

  it('should call res.status with 500 if error is not instance of UnauthorizedError and NODE_ENV != production', () => {
    errorMiddleware(NON_UNAUTHORIZED_ERROR, req, res, next)

    expect(next).not.toHaveBeenCalled()
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.status).toHaveBeenCalledTimes(1)
    expect(res.json).toHaveBeenCalledWith({
      message: NON_UNAUTHORIZED_ERROR.message,
      stack: NON_UNAUTHORIZED_ERROR.stack,
    })
    expect(res.json).toHaveBeenCalledTimes(1)
  })

  it('should call res.status with 500 if error is not instance of UnauthorizedError and NODE_ENV == production', () => {
    const NON_UNAUTHORIZED_ERROR = new Error('Some other error')
    process.env.NODE_ENV = 'production'
    errorMiddleware(NON_UNAUTHORIZED_ERROR, req, res, next)

    expect(next).not.toHaveBeenCalled()
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.status).toHaveBeenCalledTimes(1)
    expect(res.json).toHaveBeenCalledWith({
      message: NON_UNAUTHORIZED_ERROR.message,
    })
    expect(res.json).toHaveBeenCalledTimes(1)
  })
})
